package com.miracle.test.jmsactivemq.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miracle.test.jmsactivemq.sender.Producer;

@RestController
public class Controller {

	//takes the request form the web and calls the sendMessage method of controller
	@RequestMapping("/send")
	public void test(@ModelAttribute(value = "message") String message) {
		
		Producer producer = new Producer();
		producer.sendMessage(message);
		
	}
}